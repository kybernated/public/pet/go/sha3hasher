package sha3hasher

import (
	"golang.org/x/crypto/sha3"
	"hash"
	hasher "tumarov.com/go-advanced-final/utils/pkg/sha3hasher"
)

type HasherBuilder interface {
	BuildHasher(algorithm hasher.Sha3Algorithm) hash.Hash
}

type Hasher struct{}

func (h *Hasher) BuildHasher(algorithm hasher.Sha3Algorithm) hash.Hash {
	var sha3hash hash.Hash

	switch algorithm {
	case hasher.Sha3Algorithm_SHA3_224:
		sha3hash = sha3.New224()
	case hasher.Sha3Algorithm_SHA3_256:
		sha3hash = sha3.New256()
	case hasher.Sha3Algorithm_SHA3_384:
		sha3hash = sha3.New384()
	case hasher.Sha3Algorithm_SHA3_512:
		sha3hash = sha3.New512()
	}

	return sha3hash
}
