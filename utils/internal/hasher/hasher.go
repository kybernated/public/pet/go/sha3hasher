package sha3hasher

import (
	"encoding/hex"
	"github.com/sirupsen/logrus"
	"io"
	"sync"
	hasher "tumarov.com/go-advanced-final/utils/pkg/sha3hasher"
)

type Sha3Hasher struct {
	hasher.UnimplementedSha3HasherServer
	HasherBuilder HasherBuilder
	Logger        *logrus.Logger
}

func New(l *logrus.Logger) *Sha3Hasher {
	return &Sha3Hasher{HasherBuilder: &Hasher{}, Logger: l}
}

type input struct {
	index int32
	data  string
	algo  hasher.Sha3Algorithm
}

type output struct {
	index int32
	hash  string
}

func (h *Sha3Hasher) Hash(stream hasher.Sha3Hasher_HashServer) error {
	inputs, reqID, err := h.receiveStreamRequest(stream)
	if err != nil {
		return err
	}

	inputsNumber := len(inputs)

	outCh := make(chan output, inputsNumber)
	wg := &sync.WaitGroup{}
	wg.Add(inputsNumber)

	go func() {
		wg.Wait()
		close(outCh)
	}()

	for _, inputData := range inputs {
		go h.calculate(inputData, wg, outCh)
	}

	return h.sendStreamRequest(stream, reqID, outCh)
}

func (h *Sha3Hasher) calculate(inputData input, wg *sync.WaitGroup, outCh chan<- output) {
	defer wg.Done()

	sha3hash := h.HasherBuilder.BuildHasher(inputData.algo)

	// Write() method from hash.Hash interface never returns an error (@see documentation)
	_, _ = sha3hash.Write([]byte(inputData.data))

	result := sha3hash.Sum(nil)

	outCh <- output{index: inputData.index, hash: hex.EncodeToString(result)}
}

func (h *Sha3Hasher) receiveStreamRequest(stream hasher.Sha3Hasher_HashServer) (inputs []input, reqID string, err error) {
	var algo hasher.Sha3Algorithm

	logFields := logrus.Fields{}

	for {
		r, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			logFields[logrus.ErrorKey] = err
			h.Logger.WithFields(logFields).Error("receiving grpc stream failed")
			return nil, reqID, err
		}

		meta := r.GetMeta()
		if meta != nil {
			reqID = meta.GetRequestId()
			algo = meta.GetAlgorithm()
			logFields["request_id"] = reqID
			h.Logger.WithFields(logFields).Debugln("start receiving stream...")
			continue
		}

		in := r.GetInput()
		inputs = append(inputs, input{index: in.GetIndex(), data: in.GetInput(), algo: algo})
		h.Logger.WithFields(logFields).Debugf("input #%d is received", in.GetIndex())
	}

	h.Logger.WithFields(logFields).Debugln("all inputs hashes are received")

	return inputs, reqID, nil
}

func (h *Sha3Hasher) sendStreamRequest(stream hasher.Sha3Hasher_HashServer, reqID string, outCh chan output) error {
	logFields := logrus.Fields{"request_id": reqID}

	h.Logger.WithFields(logFields).Debugln("sending stream...")
	for result := range outCh {
		err := stream.Send(&hasher.Sha3Response{Output: &hasher.Output{Index: result.index, Output: result.hash}})
		if err != nil {
			logFields[logrus.ErrorKey] = err
			h.Logger.WithFields(logFields).Error("sending stream failed")
			return err
		}
	}
	h.Logger.WithFields(logFields).Debugln("stream sent")

	return nil
}
