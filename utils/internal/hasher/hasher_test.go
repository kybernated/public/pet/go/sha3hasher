package sha3hasher_test

import (
	"errors"
	logrustest "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"io"
	"testing"
	sha3hasher "tumarov.com/go-advanced-final/utils/internal/hasher"
	hasher "tumarov.com/go-advanced-final/utils/pkg/sha3hasher"
)

type mockSha3Hasher_HashServer struct {
	grpc.ServerStream
	Inputs        []*hasher.Sha3Request
	Outputs       []*hasher.Sha3Response
	WithRecvError bool
	WithSendError bool
}

func (m *mockSha3Hasher_HashServer) Recv() (*hasher.Sha3Request, error) {
	if m.WithRecvError {
		return nil, errors.New("Recv error")
	}

	if len(m.Inputs) <= 0 {
		return nil, io.EOF
	}

	input := m.Inputs[0]

	m.Inputs[0] = nil
	m.Inputs = m.Inputs[1:]

	return input, nil
}

func (m *mockSha3Hasher_HashServer) Send(output *hasher.Sha3Response) error {
	if m.WithSendError {
		return errors.New("Send error")
	}

	m.Outputs = append(m.Outputs, output)
	return nil
}

func TestHashSuccess(t *testing.T) {
	tests := []struct {
		name     string
		request  []*hasher.Sha3Request
		expected map[int32]string
	}{
		{
			name: "testing Algorithm_SHA3_256",
			request: []*hasher.Sha3Request{
				{RequestData: &hasher.Sha3Request_Meta{Meta: &hasher.Meta{RequestId: "req1-id", Algorithm: hasher.Sha3Algorithm_SHA3_256}}},
				{RequestData: &hasher.Sha3Request_Input{Input: &hasher.Input{Index: 0, Input: "string1"}}},
				{RequestData: &hasher.Sha3Request_Input{Input: &hasher.Input{Index: 1, Input: "string2"}}},
				{RequestData: &hasher.Sha3Request_Input{Input: &hasher.Input{Index: 2, Input: "string1"}}},
			},
			expected: map[int32]string{
				0: "5aebdf5dce677d272bc635110280e63512e21b6d02760470be74ead3c018fb66",
				1: "d8e0b98991e03fe6ea5334fb7c7f939b2935eaf1a61a5117ed0025fff5e2c8e6",
				2: "5aebdf5dce677d272bc635110280e63512e21b6d02760470be74ead3c018fb66",
			},
		},
		{
			name: "testing Sha3Algorithm_SHA3_224",
			request: []*hasher.Sha3Request{
				{RequestData: &hasher.Sha3Request_Meta{Meta: &hasher.Meta{RequestId: "req2-id", Algorithm: hasher.Sha3Algorithm_SHA3_224}}},
				{RequestData: &hasher.Sha3Request_Input{Input: &hasher.Input{Index: 0, Input: "string1"}}},
			},
			expected: map[int32]string{
				0: "55c6ab6cf55e8fbcaaa4d53f6d6523c05c2277e1779f13a916441235",
			},
		},
		{
			name: "testing Sha3Algorithm_SHA3_384",
			request: []*hasher.Sha3Request{
				{RequestData: &hasher.Sha3Request_Meta{Meta: &hasher.Meta{RequestId: "req3-id", Algorithm: hasher.Sha3Algorithm_SHA3_384}}},
				{RequestData: &hasher.Sha3Request_Input{Input: &hasher.Input{Index: 0, Input: "string1"}}},
			},
			expected: map[int32]string{
				0: "7e0f63ac756fe98f044ad5c707f68b41cf23d1863b3761b8cc95c906bec1a2320df505bc8bc1f8e64520acc79232d670",
			},
		},
		{
			name: "testing Sha3Algorithm_SHA3_512",
			request: []*hasher.Sha3Request{
				{RequestData: &hasher.Sha3Request_Meta{Meta: &hasher.Meta{RequestId: "req4-id", Algorithm: hasher.Sha3Algorithm_SHA3_512}}},
				{RequestData: &hasher.Sha3Request_Input{Input: &hasher.Input{Index: 0, Input: "string1"}}},
			},
			expected: map[int32]string{
				0: "002b2e220d85ddd5db77ef874f778c8c6daf19aecff65fa93b5922b93bb7a31cc5560331f450341e27c0be96d5bf696e043def74307d65adfc8f89d36db7512c",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			mockStream := &mockSha3Hasher_HashServer{Inputs: test.request}

			l, _ := logrustest.NewNullLogger()

			sha3hash := sha3hasher.New(l)

			err := sha3hash.Hash(mockStream)
			require.NoError(t, err)

			for _, output := range mockStream.Outputs {
				require.Equal(t, test.expected[output.Output.Index], output.Output.Output)
			}
		})
	}
}

func TestHashError(t *testing.T) {
	request := []*hasher.Sha3Request{
		{RequestData: &hasher.Sha3Request_Meta{Meta: &hasher.Meta{RequestId: "req1", Algorithm: hasher.Sha3Algorithm_SHA3_256}}},
		{RequestData: &hasher.Sha3Request_Input{Input: &hasher.Input{Index: 0, Input: "string1"}}},
		{RequestData: &hasher.Sha3Request_Input{Input: &hasher.Input{Index: 1, Input: "string2"}}},
		{RequestData: &hasher.Sha3Request_Input{Input: &hasher.Input{Index: 2, Input: "string1"}}},
	}

	mockStream := &mockSha3Hasher_HashServer{Inputs: request, WithRecvError: true}

	l, _ := logrustest.NewNullLogger()

	sha3hash := sha3hasher.New(l)
	err := sha3hash.Hash(mockStream)
	require.Error(t, err)

	mockStream = &mockSha3Hasher_HashServer{Inputs: request, WithSendError: true}
	sha3hash = sha3hasher.New(l)
	err = sha3hash.Hash(mockStream)
	require.Error(t, err)
}
