package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	ServiceName     string `mapstructure:"service_name"`
	Api             Api
	Grpc            Grpc
	Log             Log
	ShutdownMaxTime int `mapstructure:"shutdown_max_time"`
}

func Init() (*Config, error) {
	viper.AddConfigPath("configs")
	viper.SetConfigName("main")

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	var cfg Config

	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, err
	}

	if err := initConfigs(&cfg); err != nil {
		return nil, err
	}

	if err := parseEnv(&cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}

func initConfigs(cfg *Config) error {
	if err := cfg.Log.init(cfg); err != nil {
		return err
	}
	if err := cfg.Api.init(cfg); err != nil {
		return err
	}
	if err := cfg.Grpc.init(cfg); err != nil {
		return err
	}

	return nil
}

func parseEnv(cfg *Config) error {
	if err := godotenv.Load(); err != nil {
		return err
	}

	viper.AutomaticEnv()

	cfg.Log.parseEnv(cfg)
	cfg.Api.parseEnv(cfg)
	cfg.Grpc.parseEnv(cfg)

	return nil
}
