package config

import "github.com/spf13/viper"

type Api struct {
	Port int `mapstructure:"port"`
}

func (t *Api) init(cfg *Config) error {
	if err := viper.UnmarshalKey("api", &cfg.Api); err != nil {
		return err
	}

	return nil
}

func (t *Api) parseEnv(cfg *Config) {
}
