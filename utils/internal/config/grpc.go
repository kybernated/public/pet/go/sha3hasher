package config

import "github.com/spf13/viper"

type Grpc struct {
	Port int `mapstructure:"port"`
}

func (t *Grpc) init(cfg *Config) error {
	if err := viper.UnmarshalKey("grpc", &cfg.Grpc); err != nil {
		return err
	}

	return nil
}

func (t *Grpc) parseEnv(cfg *Config) {
}
