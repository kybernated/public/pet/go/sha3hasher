package main

import (
	"context"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/heptiolabs/healthcheck"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"tumarov.com/go-advanced-final/utils/internal/config"
	sha3hasher "tumarov.com/go-advanced-final/utils/internal/hasher"
	grpchealthcheck "tumarov.com/go-advanced-final/utils/internal/healthcheck/grpc"
	"tumarov.com/go-advanced-final/utils/internal/healthcheck/probes"
	"tumarov.com/go-advanced-final/utils/internal/logger"
	hasher "tumarov.com/go-advanced-final/utils/pkg/sha3hasher"
)

func main() {
	cfg, err := initConfig()
	if err != nil {
		log.Fatalf("config initializing failed: %v\n", err)
	}

	l := logger.NewLogrusLogger(cfg)

	hc := probes.New()

	errCh := make(chan error, 2)

	apiSrv := createApiServer(cfg, hc)
	l.Infoln("api server starting...")
	go startApi(apiSrv, errCh)

	lis, srv, err := initGrpc(cfg, l)
	if err != nil {
		l.WithError(err).Fatal("grpc server initializing failed")
	}
	l.Infoln("grpc server starting...")
	go startService(srv, lis, errCh, l)

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	hc.SetReadiness(nil)

	select {
	case err = <-errCh:
		l.WithError(err).Error("service failed")
	case <-sigCh:
		l.Warnln("system interrupting...")
	}

	shutdown(apiSrv, srv, lis, time.Duration(cfg.ShutdownMaxTime)*time.Second, l)
}

func initConfig() (*config.Config, error) {
	log.Println("config initializing...")
	cfg, err := config.Init()
	if err != nil {
		return nil, err
	}
	log.Println("config initialized")
	return cfg, err
}

func initGrpc(cfg *config.Config, l *logrus.Logger) (net.Listener, *grpc.Server, error) {
	l.Infoln("grpc server initialize starting...")

	l.Infof("tcp listener on port %d starting...", cfg.Grpc.Port)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.Grpc.Port))
	if err != nil {
		return nil, nil, err
	}
	srv := grpc.NewServer()
	l.Infoln("grpc server initialize finished")
	return lis, srv, nil
}

func createApiServer(cfg *config.Config, hc healthcheck.Handler) *http.Server {
	r := chi.NewRouter()
	r.Get("/ready", hc.ReadyEndpoint)
	r.Get("/live", hc.LiveEndpoint)

	return &http.Server{Addr: fmt.Sprintf(":%d", cfg.Api.Port), Handler: r}
}

func startApi(srv *http.Server, errCh chan<- error) {
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		errCh <- err
	}
}

func startService(srv *grpc.Server, lis net.Listener, errCh chan<- error, l *logrus.Logger) {
	sha3hash := sha3hasher.New(l)
	hasher.RegisterSha3HasherServer(srv, sha3hash)
	grpc_health_v1.RegisterHealthServer(srv, grpchealthcheck.NewHealthChecker())

	if err := srv.Serve(lis); err != nil {
		errCh <- err
	}
}

func shutdown(apiSrv *http.Server, srv *grpc.Server, lis net.Listener, shutdownMaxTime time.Duration, l *logrus.Logger) {
	l.Infoln("service is gracefully shutting down...")

	ctx, cancel := context.WithTimeout(context.Background(), shutdownMaxTime)
	defer cancel()

	l.Infoln("tcp listener closing...")
	err := lis.Close()
	if err != nil {
		l.WithError(err).Error("tcp listener closing failed")
	}
	l.Infoln("tcp listener closed")

	l.Infoln("grpc closing...")
	srv.GracefulStop()
	l.Infoln("grpc closed")

	l.Infoln("api server closing...")
	err = apiSrv.Shutdown(ctx)
	if err != nil {
		l.WithError(err).Error("api server closing failed")
	}
	l.Infoln("api server closed")

	if err != nil {
		l.Warnln("Service could not gracefully stopped")
	} else {
		l.Infoln("service gracefully stopped")
	}
}
