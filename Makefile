.PHONY: help

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

init: init-utils init-api ## Initialize all services
init-utils: ## Initialize utils service
	cd utils && cp -n .env.example .env && cd ..
init-api: ## Initialize api service
	cd api && cp -n .env.example .env && cd ..

up: tidy ## Start all services with tidy and rebuilding
	docker-compose up -d --build
down: ## Stop all services (remove containers)
	docker-compose down
restart: down up ## Restart all services with tidy and rebuilding

tidy: tidy-utils tidy-api ## Run go mod tidy for all services
tidy-utils: ## Run go mod tidy for utils service
	@cd utils && go mod tidy && cd ..
tidy-api: ## Run go mod tidy for api service
	@cd api && go mod tidy && cd ..

gen: gen-swagger gen-grpc ## Generate code for servers
gen-swagger: ## Generate code for swagger server
	@oapi-codegen --config ./api/api/oapi_config.yml ./api/api/api.yml
gen-grpc: ## Generate code for grpc server/client
	@protoc ./utils/api/proto/sha3hasher.proto --go-grpc_out=./utils/pkg --go_out=./utils/pkg

test: test-utils ## Run tests for all services
test-utils: ## Run tests for utils service
	@cd utils && go test ./... && cd ..
race: race-utils ## Run tests with data race detector for all services
race-utils: ## Run tests with data race detector for utils service
	@cd utils && go test -race ./... && cd ..
coverage: coverage-utils ## Generate code coverage report for all services
coverage-utils: ## Generate code coverage report for utils service
	@cd utils && go test -race -covermode=atomic -coverprofile=.testCoverage.txt ./... && go tool cover -func=.testCoverage.txt && cd ..

pipeline: pipeline-utils ## Prepare pipeline for all services
pipeline-utils: ## Prepare pipeline for utils service
	@cd utils && go get -v -d ./... && cd ..
