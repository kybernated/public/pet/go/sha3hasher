package main

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"tumarov.com/go-advanced-final/api/internal/config"
	dbclient "tumarov.com/go-advanced-final/api/internal/handlers/db"
	"tumarov.com/go-advanced-final/api/internal/handlers/db/postgres"
	grpchandlers "tumarov.com/go-advanced-final/api/internal/handlers/grps"
	"tumarov.com/go-advanced-final/api/internal/handlers/swagger"
	"tumarov.com/go-advanced-final/api/internal/logger"
	"tumarov.com/go-advanced-final/api/internal/probes"
	"tumarov.com/go-advanced-final/api/internal/repository/pgrepo"
)

func main() {
	cfg, err := initConfig()
	if err != nil {
		log.Fatalf("config initializing failed: %v\n", err)
	}

	l := logger.NewLogrusLogger(cfg)

	db, err := initDB(cfg, l)
	if err != nil {
		l.WithError(err).Fatal("database connection failed")
	}

	gc, err := initGrpc(cfg, l)
	if err != nil {
		l.WithError(err).Fatal("grpc connection failed")
	}

	hc := probes.New()
	hc.AddLivenessCheck("grpc", gc.Healthcheck)
	hc.AddLivenessCheck("db", db.Healthcheck)

	repositories := pgrepo.New(db)

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", cfg.Api.Port),
		Handler: swagger.New(repositories, gc, hc, l).Handler,
	}

	l.Infoln("api server starting...")
	errCh := make(chan error, 1)
	go startApi(srv, errCh)
	l.Infoln("api server started")

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	hc.SetReadiness(nil)

	select {
	case err = <-errCh:
		l.WithError(err).Errorln("api server failed")
	case <-sigCh:
		l.Warnln("system interrupting...")
	}

	shutdown(db, gc, srv, time.Duration(cfg.ShutdownMaxTime)*time.Second, l)
}

func initConfig() (*config.Config, error) {
	log.Println("config initializing...")
	cfg, err := config.Init()
	if err != nil {
		return nil, err
	}
	log.Println("config initialized")
	return cfg, err
}

func initDB(cfg *config.Config, l *logrus.Logger) (*postgresdb.DbClient, error) {
	l.Infoln("database connecting...")
	db, err := postgresdb.New(cfg)
	if err != nil {
		return nil, err
	}
	l.Infoln("database connected")
	return db, nil
}

func initGrpc(cfg *config.Config, l *logrus.Logger) (*grpchandlers.GrpcClient, error) {
	l.Infoln("grpc connecting...")
	gc, err := grpchandlers.New(cfg)
	if err != nil {
		return nil, err
	}
	l.Infoln("grpc connected")
	return gc, nil
}

func startApi(srv *http.Server, errCh chan<- error) {
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		errCh <- err
	}
}

func shutdown(db dbclient.Db, gc *grpchandlers.GrpcClient, srv *http.Server, shutdownMaxTime time.Duration, l *logrus.Logger) {
	l.Infoln("service is gracefully shutting down...")

	ctx, cancel := context.WithTimeout(context.Background(), shutdownMaxTime)
	defer cancel()

	l.Infoln("database closing...")
	err := db.Close()
	if err != nil {
		l.WithError(err).Error("database closing failed")
	} else {
		l.Infoln("database closed")
	}

	l.Infoln("api server closing...")
	err = srv.Shutdown(ctx)
	if err != nil {
		l.WithError(err).Error("api server closing failed")
	} else {
		l.Infoln("api server closed")
	}

	l.Infoln("grpc closing...")
	err = gc.Conn.Close()
	if err != nil {
		l.WithError(err).Error("grpc closing failed")
	} else {
		l.Infoln("grpc closed")
	}

	if err != nil {
		l.Warnln("Service could not gracefully stopped")
	} else {
		l.Infoln("service gracefully stopped")
	}
}
