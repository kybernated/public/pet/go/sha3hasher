package probes

import (
	"errors"
	"github.com/heptiolabs/healthcheck"
)

var readinessErr = errors.New("service is not ready yet")
var livenessErr error = nil

type HealthChecker struct {
	healthcheck.Handler
}

func New() *HealthChecker {
	hc := healthcheck.NewHandler()

	hc.AddReadinessCheck("readiness-check", func() error {
		return readinessErr
	})
	hc.AddLivenessCheck("liveness-check", func() error {
		return livenessErr
	})

	return &HealthChecker{Handler: hc}
}

func (c *HealthChecker) SetReadiness(readiness error) {
	readinessErr = readiness
}

func (c *HealthChecker) SetLiveness(liveness error) {
	livenessErr = liveness
}
