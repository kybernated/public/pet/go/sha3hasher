package grpchandlers

import (
	"context"
	"google.golang.org/grpc"
	"io"
	"time"
	"tumarov.com/go-advanced-final/utils/pkg/sha3hasher"
)

const defaultAlgorithm = sha3hasher.Sha3Algorithm_SHA3_256

type Output struct {
	Index int32
	Hash  string
}

func (h *GrpcClient) Send(inputs []string, reqID string) ([]Output, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	stream, err := createStream(ctx, h.Conn)
	if err != nil {
		return nil, err
	}

	if err = doStreamRequest(stream, inputs, reqID); err != nil {
		return nil, err
	}

	response, err := getStreamResponse(stream)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func createStream(ctx context.Context, conn *grpc.ClientConn) (sha3hasher.Sha3Hasher_HashClient, error) {
	client := sha3hasher.NewSha3HasherClient(conn)

	stream, err := client.Hash(ctx)
	if err != nil {
		return nil, err
	}

	return stream, nil
}

func doStreamRequest(stream sha3hasher.Sha3Hasher_HashClient, inputs []string, reqID string) error {
	err := stream.Send(&sha3hasher.Sha3Request{
		RequestData: &sha3hasher.Sha3Request_Meta{Meta: &sha3hasher.Meta{RequestId: reqID, Algorithm: defaultAlgorithm}},
	})
	if err != nil {
		return err
	}

	for i, input := range inputs {
		err := stream.Send(&sha3hasher.Sha3Request{
			RequestData: &sha3hasher.Sha3Request_Input{Input: &sha3hasher.Input{Index: int32(i), Input: input}},
		})
		if err != nil {
			return err
		}
	}

	if err := stream.CloseSend(); err != nil {
		return err
	}

	return nil
}

func getStreamResponse(stream sha3hasher.Sha3Hasher_HashClient) ([]Output, error) {
	var outputs []Output

	for {
		outputData, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		output := outputData.GetOutput()
		outputs = append(outputs, Output{Index: output.GetIndex(), Hash: output.GetOutput()})
	}

	return outputs, nil
}
