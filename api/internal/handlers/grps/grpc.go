package grpchandlers

import (
	"context"
	"errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"time"
	"tumarov.com/go-advanced-final/api/internal/config"
)

type GrpcClient struct {
	Conn   *grpc.ClientConn
	Config *config.Config
}

func New(cfg *config.Config) (*GrpcClient, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(cfg.Grpc.ConnectionTimeout)*time.Second)
	defer cancel()

	conn, err := grpc.DialContext(ctx, cfg.Grpc.Utils.Target, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	return &GrpcClient{Conn: conn, Config: cfg}, nil
}

func (h *GrpcClient) Healthcheck() error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	client := grpc_health_v1.NewHealthClient(h.Conn)

	req := &grpc_health_v1.HealthCheckRequest{}

	res, err := client.Check(ctx, req)
	if err != nil {
		return err
	}

	if res.GetStatus() != grpc_health_v1.HealthCheckResponse_SERVING {
		return errors.New("grpc server not responds")
	}

	return nil
}
