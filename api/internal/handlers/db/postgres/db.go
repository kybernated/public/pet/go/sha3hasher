package postgresdb

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	_ "github.com/lib/pq"
	"github.com/pressly/goose"
	"net/url"
	"time"
	"tumarov.com/go-advanced-final/api/internal/config"
)

type DbClient struct {
	Pool   *pgxpool.Pool
	Config *config.Config
}

func New(cfg *config.Config) (*DbClient, error) {
	connStr := fmt.Sprintf("%s://%s:%s@%s:%s/%s?sslmode=disable&connect_timeout=%d",
		"postgres",
		url.QueryEscape(cfg.Db.User),
		url.QueryEscape(cfg.Db.Password),
		cfg.Db.Host,
		cfg.Db.Port,
		cfg.Db.DBName,
		cfg.Db.ConnectionTimeout)

	poolConfig, _ := pgxpool.ParseConfig(connStr)
	poolConfig.MaxConns = cfg.Db.PoolMaxConn

	pool, err := pgxpool.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		return nil, err
	}

	db, err := sql.Open("postgres", poolConfig.ConnString())
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}

	err = goose.Up(db, "./database/postgres/migrations")
	if err != nil {
		return nil, err
	}

	return &DbClient{Pool: pool, Config: cfg}, nil
}

func (c *DbClient) Healthcheck() error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	if err := c.Pool.Ping(ctx); err != nil {
		return err
	}

	return nil
}

func (c *DbClient) Close() error {
	c.Pool.Close()
	return nil
}
