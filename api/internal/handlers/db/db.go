package dbclient

type Db interface {
	Close() error
	Healthcheck() error
}
