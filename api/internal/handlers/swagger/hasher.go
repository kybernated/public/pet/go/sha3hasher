package swagger

import (
	"context"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"net/http"
	"sort"
	"strconv"
	"time"
	"tumarov.com/go-advanced-final/api/internal/handlers/middlewares"
	swaggerhandlers "tumarov.com/go-advanced-final/api/internal/handlers/swagger/generated"
)

func (h *ApiServer) GetCheck(w http.ResponseWriter, r *http.Request, params swaggerhandlers.GetCheckParams) {
	w.Header().Set("Content-Type", "application/json")
	reqID := r.Context().Value(middlewares.RequestIDContextKey).(string)

	logFields := logrus.Fields{"request_id": reqID}

	h.Logger.WithFields(logFields).Debugln("handling GET /check request")

	h.Logger.WithFields(logFields).Debugln("decoding input params")
	ids := make([]int, len(params.Ids))
	for i, idStr := range params.Ids {
		id, err := strconv.Atoi(idStr)
		if err != nil {
			logFields[logrus.ErrorKey] = err
			h.Logger.WithFields(logFields).Errorln("could not decode input params")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		ids[i] = id
	}

	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)
	defer cancel()

	h.Logger.WithFields(logFields).Debugln("fetching hashes")
	hashes, err := h.Repositories.Hash.GetByIds(ctx, ids)
	if err != nil {
		logFields[logrus.ErrorKey] = err
		h.Logger.WithFields(logFields).Errorln("could not fetch hashes by ids")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	h.Logger.WithFields(logFields).Debugln("converting response to json")
	response, err := json.Marshal(hashes)
	if err != nil {
		logFields[logrus.ErrorKey] = err
		h.Logger.WithFields(logFields).Errorln("could not json marshal the hashes")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	h.Logger.WithFields(logFields).Debugln("writing response")
	_, err = w.Write(response)
	if err != nil {
		logFields[logrus.ErrorKey] = err
		h.Logger.WithFields(logFields).Errorln("could not write response")
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (h *ApiServer) PostSend(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	reqID := r.Context().Value(middlewares.RequestIDContextKey).(string)

	logFields := logrus.Fields{"request_id": reqID}

	h.Logger.WithFields(logFields).Debugln("handling POST /send request")

	h.Logger.WithFields(logFields).Debugln("decoding input params")

	var inputs []string

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&inputs)
	if err != nil {
		logFields[logrus.ErrorKey] = err
		h.Logger.WithFields(logFields).Errorln("could not decode input params")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	h.Logger.WithFields(logFields).Debugln("calculating hashes")
	outputs, err := h.GrpcClient.Send(inputs, reqID)
	if err != nil {
		logFields[logrus.ErrorKey] = err
		h.Logger.WithFields(logFields).Errorln("could not calculate hashes")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	h.Logger.WithFields(logFields).Debugln("sorting calculated hashes")
	sort.Slice(outputs, func(i, j int) bool {
		return outputs[i].Index < outputs[j].Index
	})

	var hashesStrings []string
	for _, output := range outputs {
		hashesStrings = append(hashesStrings, output.Hash)
	}

	h.Logger.WithFields(logFields).Debugln("saving calculated hashes")
	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)
	defer cancel()
	hashes, err := h.Repositories.Hash.Save(ctx, hashesStrings)
	if err != nil {
		logFields[logrus.ErrorKey] = err
		h.Logger.WithFields(logFields).Errorln("could not save hashes")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	h.Logger.WithFields(logFields).Debugln("converting response to json")
	response, err := json.Marshal(hashes)
	if err != nil {
		logFields[logrus.ErrorKey] = err
		h.Logger.WithFields(logFields).Errorln("could not json marshal the hashes")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	h.Logger.WithFields(logFields).Debugln("writing response")
	_, err = w.Write(response)
	if err != nil {
		logFields[logrus.ErrorKey] = err
		h.Logger.WithFields(logFields).Errorln("could not write response")
		w.WriteHeader(http.StatusInternalServerError)
	}
}
