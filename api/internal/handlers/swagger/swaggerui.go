package swagger

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	v4 "github.com/swaggest/swgui/v4emb"
	"net/http"
	"os"
)

func RoutesSwaggerUI(prefix string, r *chi.Mux) {
	h := v4.New("My API", fmt.Sprintf("%s/api.yml", prefix), fmt.Sprintf("%s", prefix))

	r.Method(http.MethodGet, fmt.Sprintf("%s", prefix), h)
	r.Method(http.MethodGet, fmt.Sprintf("%s/{anything:.*}", prefix), h)

	r.Get(fmt.Sprintf("%s/api.yml", prefix), func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/x-yaml")
		dat, err := os.ReadFile("api/api.yml")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		_, err = w.Write(dat)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})
}
