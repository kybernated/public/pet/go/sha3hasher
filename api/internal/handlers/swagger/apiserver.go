package swagger

import (
	"github.com/go-chi/chi/v5"
	"github.com/heptiolabs/healthcheck"
	"github.com/sirupsen/logrus"
	"net/http"
	grpchandlers "tumarov.com/go-advanced-final/api/internal/handlers/grps"
	"tumarov.com/go-advanced-final/api/internal/handlers/middlewares"
	swaggerhandlers "tumarov.com/go-advanced-final/api/internal/handlers/swagger/generated"
	"tumarov.com/go-advanced-final/api/internal/repository"
)

type ApiServer struct {
	GrpcClient   *grpchandlers.GrpcClient
	Repositories *repository.Repositories
	Handler      http.Handler
	Logger       *logrus.Logger
}

func New(repositories *repository.Repositories, gc *grpchandlers.GrpcClient, hc healthcheck.Handler, l *logrus.Logger) *ApiServer {
	r := chi.NewRouter()
	r.Use(middlewares.ContextRequestMiddleware)
	r.Get("/ready", hc.ReadyEndpoint)
	r.Get("/live", hc.LiveEndpoint)

	RoutesSwaggerUI("/swaggerui", r)

	s := &ApiServer{GrpcClient: gc, Repositories: repositories, Logger: l}
	s.Handler = swaggerhandlers.HandlerFromMux(s, r)

	return s
}
