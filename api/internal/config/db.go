package config

import "github.com/spf13/viper"

type DB struct {
	Postgres
}

type Postgres struct {
	PoolMaxConn       int32  `mapstructure:"pool_max_conn"`
	ConnectionTimeout int    `mapstructure:"connection_timeout"`
	User              string `mapstructure:"user"`
	Password          string `mapstructure:"password"`
	Host              string `mapstructure:"host"`
	Port              string `mapstructure:"port"`
	DBName            string `mapstructure:"name"`
}

func (t *DB) init(cfg *Config) error {
	if err := viper.UnmarshalKey("db.postgres", &cfg.Db); err != nil {
		return err
	}

	return nil
}

func (t *DB) parseEnv(cfg *Config) {
	if viper.IsSet("DB_USER") {
		cfg.Db.User = viper.GetString("DB_USER")
	}
	if viper.IsSet("DB_PASSWORD") {
		cfg.Db.Password = viper.GetString("DB_PASSWORD")
	}
	if viper.IsSet("DB_HOST") {
		cfg.Db.Host = viper.GetString("DB_HOST")
	}
	if viper.IsSet("DB_PORT") {
		cfg.Db.Port = viper.GetString("DB_PORT")
	}
	if viper.IsSet("DB_NAME") {
		cfg.Db.DBName = viper.GetString("DB_NAME")
	}
}
