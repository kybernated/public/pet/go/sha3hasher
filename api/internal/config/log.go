package config

import "github.com/spf13/viper"

type Log struct {
	Graylog
	Level string `mapstructure:"level"`
}

type Graylog struct {
	Host string `mapstructure:"host"`
	Port int    `mapstructure:"port"`
}

func (t *Log) init(cfg *Config) error {
	if err := viper.UnmarshalKey("log", &cfg.Log); err != nil {
		return err
	}
	if err := viper.UnmarshalKey("log.graylog", &cfg.Log.Graylog); err != nil {
		return err
	}

	return nil
}

func (t *Log) parseEnv(cfg *Config) {
	if viper.IsSet("LOG_GRAYLOG_HOST") {
		cfg.Log.Graylog.Host = viper.GetString("LOG_GRAYLOG_HOST")
	}
	if viper.IsSet("LOG_GRAYLOG_PORT") {
		cfg.Log.Graylog.Port = viper.GetInt("LOG_GRAYLOG_PORT")
	}
	if viper.IsSet("LOG_LEVEL") {
		cfg.Log.Level = viper.GetString("LOG_LEVEL")
	}
}
