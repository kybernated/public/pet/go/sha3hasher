package config

import "github.com/spf13/viper"

type Grpc struct {
	ConnectionTimeout int `mapstructure:"connection_timeout"`
	Utils
}

type Utils struct {
	Target string `mapstructure:"target"`
}

func (t *Grpc) init(cfg *Config) error {
	if err := viper.UnmarshalKey("grpc", &cfg.Grpc); err != nil {
		return err
	}

	if err := viper.UnmarshalKey("grpc.utils", &cfg.Grpc.Utils); err != nil {
		return err
	}

	return nil
}

func (t *Grpc) parseEnv(cfg *Config) {

}
