package logger

import (
	"fmt"
	formatter "github.com/fabienm/go-logrus-formatters"
	graylog "github.com/gemnasium/logrus-graylog-hook/v3"
	"github.com/go-errors/errors"
	"github.com/sirupsen/logrus"
	"tumarov.com/go-advanced-final/api/internal/config"
)

func NewLogrusLogger(cfg *config.Config) *logrus.Logger {
	l := logrus.New()
	l.SetFormatter(formatter.NewGelf(cfg.ServiceName))

	switch cfg.Log.Level {
	case "debug":
		l.SetLevel(logrus.TraceLevel)
	case "info":
		l.SetLevel(logrus.InfoLevel)
	case "error":
		l.SetLevel(logrus.ErrorLevel)
	default:
		l.SetLevel(logrus.InfoLevel)
	}

	l.AddHook(&ErrorHook{})
	l.AddHook(graylog.NewGraylogHook(
		fmt.Sprintf("%s:%d", cfg.Log.Graylog.Host, cfg.Log.Graylog.Port),
		map[string]any{
			"service_name": cfg.ServiceName,
		},
	))

	return l
}

type ErrorHook struct{}

func (h *ErrorHook) Levels() []logrus.Level {
	return []logrus.Level{logrus.ErrorLevel, logrus.FatalLevel, logrus.PanicLevel}
}

func (h *ErrorHook) Fire(e *logrus.Entry) error {
	if err, ok := e.Data[logrus.ErrorKey]; ok {
		er := errors.New(err)
		e.Data["trace"] = er.ErrorStack()
	}
	return nil
}
