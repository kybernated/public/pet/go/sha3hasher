package pgrepo

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"strconv"
	"strings"
	handlers "tumarov.com/go-advanced-final/api/internal/handlers/swagger/generated"
)

const tableName = "hashes"

type HashRepository struct {
	db *pgxpool.Pool
}

func NewHashRepository(db *pgxpool.Pool) *HashRepository {
	return &HashRepository{db: db}
}

func (h *HashRepository) Save(ctx context.Context, hashes []string) (handlers.ArrayOfHash, error) {
	query := "INSERT INTO " + tableName + " (hash) VALUES "
	var placeholders []string
	for i := range hashes {
		placeholders = append(placeholders, "($"+strconv.Itoa(i+1)+")")
	}
	query += strings.Join(placeholders, ",")
	query += " RETURNING id, hash;"

	params := make([]interface{}, len(hashes))
	for i := range hashes {
		params[i] = hashes[i]
	}
	rows, err := h.db.Query(ctx, query, params...)
	if err != nil {
		return nil, err
	}

	if err == pgx.ErrNoRows {
		return nil, pgx.ErrNoRows
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	response := handlers.ArrayOfHash{}

	for rows.Next() {
		hash := handlers.Hash{}
		if err = rows.Scan(&hash.Id, &hash.Hash); err != nil {
			return nil, err
		}
		response = append(response, hash)
	}

	return response, nil
}

func (h *HashRepository) GetByIds(ctx context.Context, ids []int) (handlers.ArrayOfHash, error) {
	query := "SELECT id, hash FROM " + tableName + " WHERE id in ("
	var placeholders []string
	for i := range ids {
		placeholders = append(placeholders, "$"+strconv.Itoa(i+1))
	}
	query += strings.Join(placeholders, ",") + ");"
	params := make([]interface{}, len(ids))
	for i := range ids {
		params[i] = ids[i]
	}

	rows, err := h.db.Query(ctx, query, params...)
	if err != nil {
		return nil, err
	}

	if err == pgx.ErrNoRows {
		return nil, pgx.ErrNoRows
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	response := handlers.ArrayOfHash{}

	for rows.Next() {
		hash := handlers.Hash{}
		if err = rows.Scan(&hash.Id, &hash.Hash); err != nil {
			return nil, err
		}
		response = append(response, hash)
	}

	return response, nil
}
