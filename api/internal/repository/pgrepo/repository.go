package pgrepo

import (
	"tumarov.com/go-advanced-final/api/internal/handlers/db/postgres"
	"tumarov.com/go-advanced-final/api/internal/repository"
)

func New(db *postgresdb.DbClient) *repository.Repositories {
	return &repository.Repositories{
		Hash: NewHashRepository(db.Pool),
	}
}
