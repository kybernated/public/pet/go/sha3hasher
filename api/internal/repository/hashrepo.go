package repository

import (
	"context"
	handlers "tumarov.com/go-advanced-final/api/internal/handlers/swagger/generated"
)

type HashRepository interface {
	Save(ctx context.Context, hashes []string) (handlers.ArrayOfHash, error)
	GetByIds(ctx context.Context, ids []int) (handlers.ArrayOfHash, error)
}
