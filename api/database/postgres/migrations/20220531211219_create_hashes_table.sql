-- +goose Up
-- +goose StatementBegin
create table hashes
(
    id   serial,
    hash varchar(512) not null
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table hashes;
-- +goose StatementEnd
