# Итоговое задание практикума GO-ADVANCED [![pipeline status](https://gitlab.com/kybernated/public/pet/go/sha3hasher/badges/main/pipeline.svg)](https://gitlab.com/kybernated/public/pet/go/sha3hasher/-/commits/main) [![coverage report](https://gitlab.com/kybernated/public/pet/go/sha3hasher/badges/main/coverage.svg)](https://gitlab.com/kybernated/public/pet/go/sha3hasher/-/commits/main)

## Описание

Проект состоит из 2ух основных сервисов:  
- `api` - REST api сервис
- `utils` - сервис с полезными функциями

Также при запуске проекта будут развернуты дополнительные сервисы:
- `postgres` - БД для хранения уже посчитанных хешей строк.
- `graylog` - сервис управления логами.
- `elasticsearch`, `mongo` - необходимы для graylog - собственно для хранения логов.

В каждом сервисе реализованы gracefull shutdown, сквозное логирование, логирование stack trace для ошибок.

### api
Сервис имеет след. эндпойнты:
- **POST /send** - хеширование строк. В ответ отдаются хеши исходных строк с присвоенными им id. По этим id можно получить их уже посчитанные и сохраненные в БД хеши.
- **GET /check** - получение из БД заранее посчитанных хешей строк по id
- **GET /live** - liveness probe сервиса
- **GET /ready** - readiniess probe сервиса
- **GET /swaggerui** - web-интерфейс SwaggerUI

### utils
Сервис имеет след. эндпойнты:
- **GET /live** - liveness probe сервиса
- **GET /ready** - readiniess probe сервиса

Общение с другими сервисами осуществляется через gRPC.

Реализованы инструменты:  
- **sha3 hasher**: по входящей строке высчитывает хэш по заданному алгоритму.  
    Данные в хешер поступают через grpc stream.  
    Предполагается, что сначала поступают метаданные:  
      - _id запроса_ - используется для сквозного логирования  
      - _алгоритм_ - один из 4 алгоритмов, по которому будет считаться sha3 хеш (sha3-224, sha3-256, sha3-384, sha3-512)  
    Затем поступают строки для хеширования (каждая строка имеет индекс, для того, чтобы клиент смог сопоставить пришедший в ответ хеш и исходную строку).  
    В ответ отдаются хеши строк с индексами исходных строк.

## Запуск
В корне проекта имеется Makefile для удобного управления.
При вводе команды `make` выведется помощь по всем командам.

```shell
help                           Show this help
init                           Initialize all services
init-utils                     Initialize utils service
init-api                       Initialize api service
up                             Start all services with tidy and rebuilding
down                           Stop all services (remove containers)
restart                        Restart all services with tidy and rebuilding
tidy                           Run go mod tidy for all services
tidy-utils                     Run go mod tidy for utils service
tidy-api                       Run go mod tidy for api service
gen                            Generate code for servers
gen-swagger                    Generate code for swagger server
gen-grpc                       Generate code for grpc server/client
test                           Run tests for all services
test-utils                     Run tests for utils service
race                           Run tests with data race detector for all services
race-utils                     Run tests with data race detector for utils service
coverage                       Generate code coverage report for all services
coverage-utils                 Generate code coverage report for utils service
```

Перед стартом проекта необходимо его инициализировать, запустив команду `make init`.  
Если необходимо, измените переменные окружения в .env файлах каждого сервиса.
Далее стартуем проект командой `make up`. 

## Использование
После запуска проекта, для значений по-умолчанию в переменных окружения, будет доступен сервис `api` по адресу `localhost:8800`.
Ниже приведены примеры запросов:
```shell
# Создание хешей
curl --location --request POST 'http://localhost:8800/send' \
--header 'Content-Type: application/json' \
--data-raw '[
    "Строка 1",
    "Строка 2"
]'
```
```shell
# Получение хешей по id
curl --location --request GET 'http://localhost:8800/check?ids=1&ids=2'
```

Также можно воспользоваться web-интерфейсом Swagger UI по адресу http://localhost:8800/swaggerui